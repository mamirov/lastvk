package com.mamirov.lastvk.player;


import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.IOException;
import java.io.InputStream;

public class VKPlayer {

    public Player player;
    public InputStream mpStream;

    public VKPlayer (InputStream inputStream){
        try {
            player = new Player(inputStream);
            mpStream = inputStream;
        } catch (JavaLayerException e) {
            e.printStackTrace();
        }
    }

    public int STOP = 0;
    public int PLAY = 1;
    public int PAUSE = 2;
    public int RESUME = 3;
    public int STATE = PLAY;
    public int position;

    public void play(){
        Thread playThread = new Thread(new Runnable() {
            @Override
            public void run() {
                STATE = PLAY;
                try {
                    while(STATE == PLAY){
                        player.play(1);
                        position = player.getPosition();
                        if(player.isComplete()){
                            System.out.println("WIIIIIIIIIIIIIIN!!!!!!!!!");
                            STATE = STOP;
                        }
                    }
                } catch (JavaLayerException e) {
                    e.printStackTrace();
                }
            }
        });
        playThread.start();
    }

    public void pause(){
        Thread pauseThread = new Thread(new Runnable() {
            @Override
            public void run() {
                STATE = PAUSE;
                System.out.println(player.getPosition());
            }
        });
        pauseThread.start();

    }

    public void stop(){
        Thread stopThread = new Thread(new Runnable() {
            @Override
            public void run() {
                STATE = STOP;
                try {
                    mpStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        stopThread.start();
    }
}

package com.mamirov.lastvk.player;


import com.mamirov.lastvk.Config;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VKAudioList {

    public static List<JsonNode> getAudioList() throws IOException {
        URL url = new URL("https://api.vkontakte.ru/method/audio.get?access_token="+ Config.getVKAuthToken());
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(url);
        JsonNode response = jsonNode.get("response");
        List<JsonNode> audioList = new ArrayList<JsonNode>();
        for(Iterator i = response.getElements(); i.hasNext();){
            JsonNode audio = (JsonNode) i.next();
            audioList.add(audio);
        }
        return audioList;
    }

    public static void main (String [] args) throws IOException {
        for(JsonNode node : getAudioList()){
            System.out.println(node.get("artist"));
            System.out.println(node.get("title"));
            System.out.println(node.get("url"));

        }
    }
}

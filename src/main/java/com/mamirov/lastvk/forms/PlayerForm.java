package com.mamirov.lastvk.forms;

import com.mamirov.lastvk.player.VKPlayer;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class PlayerForm {
    private JPanel PlayerForm;
    private JButton playButton;
    private JButton pauseButton;
    private JProgressBar progressBar1;
    private JButton stopButton;
    private VKPlayer vkPlayer;
    public PlayerForm(final String mpUrl) throws IOException, UnsupportedAudioFileException {
        if(!(mpUrl == null)){
            getAudioConnection(mpUrl).connect();
            final InputStream mp3Stream = getAudioConnection(mpUrl).getInputStream();
            vkPlayer = new VKPlayer(mp3Stream);
            pauseButton.setVisible(false);
            playButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    play(mpUrl);
                }
            });
            pauseButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pause();
                }
            });
            stopButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    stop();
                }
            });
        }

    }

    public void play(String mp3Url){
        System.out.println(vkPlayer.STATE);
        if(vkPlayer.STATE == 0){
            try {
                vkPlayer = new VKPlayer(getAudioConnection(mp3Url).getInputStream());
                vkPlayer.play();
                pauseButton.setVisible(true);
                playButton.setVisible(false);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        else if(vkPlayer.STATE == 1 || vkPlayer.STATE == 2){
            vkPlayer.play();
            playButton.setVisible(false);
            pauseButton.setVisible(true);
        }
    }

    public void pause(){
        vkPlayer.pause();
        pauseButton.setVisible(false);
        playButton.setVisible(true);
    }

    public void stop(){
        vkPlayer.stop();
        pauseButton.setVisible(false);
        playButton.setVisible(true);
    }

    public void setProgressBar(int duration){
        progressBar1.setMaximum(duration);
        progressBar1.setMinimum(0);
        final Thread progressThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(vkPlayer.position<Integer.MAX_VALUE){
                    progressBar1.setValue(vkPlayer.position);
                }
            }
        });
        progressThread.start();

        progressBar1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                progressBar1.setValue(vkPlayer.position);
            }
        });
    }

    public URLConnection getAudioConnection(String mp3Url){
        try {
            URL url = new URL(mp3Url);
            return url.openConnection();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JPanel getPlayer(){
        return PlayerForm;
    }
}

package com.mamirov.lastvk.forms;


import com.mamirov.lastvk.Config;
import com.mamirov.lastvk.player.VKAudioList;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.codehaus.jackson.JsonNode;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.List;

public class MainForm {
    private JPanel MainForm;
    private JButton loginButton;
    private JList audioListFrame;
    private JLabel song;
    private JScrollPane scrollPane;
    private JPanel player;
    private static WebEngine webEngine;
    private static String access_token;
    private static String user_id;
    private static JFrame mainFrame;
    public String mp3Url;

    public MainForm() throws IOException, UnsupportedAudioFileException {
        loginButton.setVisible(false);
        scrollPane.setVisible(false);
        if(Config.getVKAuthToken() == null || Config.getVKAuthToken().isEmpty()){
            mainFrame.setTitle("Login");
            loginButton.setVisible(true);
            loginButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    initAndShowGUI();
                }
            });
        }
        else{
            final List<JsonNode> audioList = VKAudioList.getAudioList();
            mp3Url = audioList.get(0).get("url").asText();
            song.setText(audioList.get(0).get("artist")+" - "+audioList.get(0).get("title"));
            final PlayerForm vkPlayer = new PlayerForm(mp3Url);
            vkPlayer.setProgressBar(Integer.parseInt(audioList.get(0).get("duration").asText())*1000);
            mainFrame.setTitle("Player");
            scrollPane.setVisible(true);
            player.add(vkPlayer.getPlayer());


            DefaultListModel listModel = new DefaultListModel();
            audioListFrame.setModel(listModel);

            for(JsonNode audio : audioList){
                listModel.addElement(audio.get("artist")+" - "+audio.get("title"));
            }

            audioListFrame.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    JList list = (JList) e.getSource();
                    if (e.getClickCount() == 2) {
                        int index = list.locationToIndex(e.getPoint());
                        if(!song.getText().equals(audioListFrame.getSelectedValue().toString())){
                            vkPlayer.stop();
                            song.setText(audioListFrame.getSelectedValue().toString());
                            for(JsonNode audio : audioList){
                                if(audioListFrame.getSelectedValue().toString().contains(audio.get("title").asText())){
                                    mp3Url = audio.get("url").asText();
                                    vkPlayer.setProgressBar(Integer.parseInt(audio.get("duration").asText())*1000);
                                    vkPlayer.play(mp3Url);
                                    System.out.println(audio.get("url"));
                                    System.out.println(audio.get("duration"));
                                }
                            }
                        }
                    } else if (e.getClickCount() == 3) {   // Triple-click
                        int index = list.locationToIndex(e.getPoint());

                    }
                    super.mouseClicked(e);
                }
            });
        }
    }

    public static void initAndShowGUI(){
        mainFrame.setVisible(false);
        final JFrame frame = new JFrame("FX");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().setLayout(null); // do the layout manually

        final JFXPanel fxPanel = new JFXPanel();

        frame.add(fxPanel);
        frame.setVisible(true);

        fxPanel.setSize(new Dimension(700, 500));
        fxPanel.setLocation(new Point(0, 27));

        frame.getContentPane().setPreferredSize(new Dimension(700, 500));
        frame.pack();
        frame.setResizable(false);

        fxPanel.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                if(webEngine.getLocation().contains("access_token")){
                    String url = webEngine.getLocation();
                    System.out.println(url);
                    access_token = url.split("#")[1].split("&")[0].split("=")[1];
                    user_id = url.split("#")[1].split("&")[2].split("=")[1];
                    System.out.println(access_token + " " + user_id);
                    Config.addToken(access_token, user_id);
                    frame.setVisible(false);
                    try {
                        mainFrame.setContentPane(new MainForm().MainForm);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    mainFrame.pack();
                    mainFrame.setVisible(true);
                }

            }
        });

        Platform.runLater(new Runnable() { // this will run initFX as JavaFX-Thread
            @Override
            public void run() {
                initFX(fxPanel);
            }
        });
    }

    private static void initFX(final JFXPanel fxPanel){
        Group group = new Group();
        Scene scene = new Scene(group);
        fxPanel.setScene(scene);

        WebView webView = new WebView();

        group.getChildren().add(webView);
        webView.setMinSize(700, 500);
        webView.setMaxSize(700, 500);

        // Obtain the webEngine to navigate
        webEngine = webView.getEngine();
        webEngine.load("https://oauth.vk.com/authorize?client_id=3979888&scope=audio,offline&refirect_uri=https://oauth.vk.com/blank.html&display=popup&response_type=token");
    }

    public static void main(String[] args) throws IOException, UnsupportedAudioFileException {
        Config.createConfigDir();
        mainFrame = new JFrame("Login");
        mainFrame.setContentPane(new MainForm().MainForm);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }
}

package com.mamirov.lastvk;

import com.mamirov.lastvk.forms.MainForm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private static File dir = new File(System.getProperty("user.home")+"/lastvk");
    private static String propPath = dir.getAbsolutePath()+"/config.properties";

    public static Properties props = new Properties();
    static {
        try {
            createConfigDir();
            FileInputStream fis = new FileInputStream(propPath);
            props.load(fis);
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addToken(String vkAuthToken, String vkUserId){
        try {
            createConfigDir();
            props.setProperty("vk.auth.token", vkAuthToken);
            props.setProperty("vk.user.id", vkUserId);
            props.store(new FileOutputStream(propPath), "Added Auth Token");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getVKAuthToken(){
        return props.getProperty("vk.auth.token");
    }

    public static String getVKUserId(){
        return props.getProperty("vk.user.id");
    }

    public static void createConfigDir(){
        try {
            dir.mkdir();
            File propFile = new File(propPath);
            propFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
